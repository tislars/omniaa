node {
    try {
        notifyBuild('STARTED')
        stage('Preparation') {
    		checkout scm
            sh 'cp phpunit.xml.dist phpunit.xml'
        }
    	stage('Build') {
    		sh 'mkdir -p app/cache app/logs web/media/cache web/uploads'
    		sh 'composer install'
            sh 'chmod +x tools/phpunit.phar'
    		sh 'chmod +x app/console'
    	}
    	stage('Test') {
    		sh 'ant'
    		step([
    		    $class: 'CloverPublisher',
    		    cloverReportDir: 'build/coverage',
                cloverReportFileName: 'build/logs/clover.xml',
                healthyTarget: [methodCoverage: 10, conditionalCoverage: 50, statementCoverage: 10], // optional, default is: method=70, conditional=80, statement=80
                unhealthyTarget: [methodCoverage: 5, conditionalCoverage: 25, statementCoverage: 5], // optional, default is none
                failingTarget: [methodCoverage: 0, conditionalCoverage: 0, statementCoverage: 0]     // optional, default is none
            ])
            step([
                $class: 'hudson.plugins.checkstyle.CheckStylePublisher', 
                pattern: 'build/logs/checkstyle.xml'
            ])
    	}
    	stage('Deploy') {
    	    sh 'bin/mage deploy to:production'
    	}
    } catch (e) {
        currentBuild.result = "FAILED"
        throw e
    } finally {
        stage('Notify') {
            notifyBuild(currentBuild.result)
        }
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
	// build status of null means successful
	buildStatus =  buildStatus ?: 'SUCCESSFUL'

	// Default values
	def colorName = 'RED'
	def colorCode = '#FF0000'
	def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
	def summary = "${subject} (${env.BUILD_URL})"
	def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
	<p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

	if (buildStatus == 'STARTED') {
		color = 'YELLOW'
		colorCode = '#FFFF00'
	} else if (buildStatus == 'SUCCESSFUL') {
		color = 'GREEN'
		colorCode = '#00FF00'
	} else {
		color = 'RED'
		colorCode = '#FF0000'
	}

	// Send notifications
	slackSend (color: colorCode, message: summary)
}