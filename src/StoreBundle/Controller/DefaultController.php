<?php

namespace StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StoreBundle\Util\Calculator;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $calc = new Calculator();
        $result = $calc->multiply(10, 4);
        
        return $this->render(
            'StoreBundle:Default:index.html.twig',
            array(
                'result' => $result,
            )
        );
    }
}
