<?php

namespace StoreBundle\Util;
/**
 * Created by PhpStorm.
 * User: larsh
 * Date: 17-1-2017
 * Time: 20:07
 */

class Calculator {
    
    public function multiply($a, $b)
    {
        return $a * $b;
    }

    public function sum($a, $b) {
        return $a + $b;
    }

    public function divide($a, $b)
    {
        if ($b === 0)
        {
            return 'NaN';
        }
        
        return $a / $b;
    }

    public function subtraction($a, $b) {
        return $a - $b;
    }
}