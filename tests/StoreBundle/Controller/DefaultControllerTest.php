<?php

namespace StoreBundle\Tests\Controller;

use StoreBundle\Util\Calculator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $a = 5;
        $b = 6;
        $result = $a * $b;

        $this->assertEquals(30, $result);
    }

    public function testCalculator()
    {
        $calc = new Calculator();
        $a = 3;
        $b = 5;

        $result = $calc->divide($a, $b);
        $this->assertEquals($a / $b, $result);

        $result = $calc->multiply($a, $b);
        $this->assertEquals($a * $b, $result);

        $result = $calc->subtraction($a, $b);
        $this->assertEquals($a - $b, $result);

        $result = $calc->sum($a, $b);
        $this->assertEquals($a + $b, $result);
    }

    public function testDate() {
        $date = new \DateTime('now');

        $this->assertGreaterThan('2017-01-01', $date->format('Y-m-d'));
    }
}
